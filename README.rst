Custom VCF to FASTA Script (Broad)
==================================

Version: 0.3

This is a GeneFlow app that wraps a custom VCF to FASTA script from Broad.

Inputs
------

1. input: VCF file to convert to FASTA.

Parameters
----------

1. max_amb_samples: Max number of samples with ambiguous calls for inclusion. Default: 0.
2. max_perc_amb_samples: Max percent of samples with ambiguous calls for inclusion. Default: 10.
3. output: Directory to contain the FASTA file.


